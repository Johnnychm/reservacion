import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () =>
      import('./home/home.module').then((m) => m.HomePageModule),
  },
  {
    path: '',
    redirectTo: 'catalogo',
    pathMatch: 'full',
  },
  {
    path: 'catalogo',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./catalogo/catalogo.module').then(
            (m) => m.CatalogoPageModule
          ),
      },
      {
        path: ':catalogoId',
        loadChildren: () =>
          import('./catalogo/detalle/detalle.module').then(
            (m) => m.DetallePageModule
          ),
      },
      {
        path: 'agregar',
        loadChildren: () =>
          import('./catalogo/agregar/agregar.module').then(
            (m) => m.AgregarPageModule
          ),
      },
      {
        path: ':catalogoId/editar',
        loadChildren: () =>
          import('./catalogo/editar/editar.module').then(
            (m) => m.EditarPageModule
          ),
      },
      {
        path: 'editar',
        children: [
          {
            path: ':catalogoId',
            loadChildren: () =>
              import('./catalogo/editar/editar.module').then(
                (m) => m.EditarPageModule
              ),
          },
        ],
      },
    ],
  },
  {
    path: 'login',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./login/login.module').then((m) => m.LoginPageModule),
      },
      {
        path: 'agregar',
        loadChildren: () =>
          import('./login/agregar/agregar.module').then(
            (m) => m.AgregarPageModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
