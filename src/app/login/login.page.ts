import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from './login.model';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  user: User;
  constructor(
    private activatedRoute: ActivatedRoute,
    private loginService: LoginService,
    private router: Router
  ) {
    this.user = loginService.loggedUser;
  }

  ngOnInit() {
    if (this.loginService.loggedUser === undefined) {
      this.router.navigate(['login/login']);
    } else {
      this.activatedRoute.paramMap.subscribe((paramMap) => {
        if (!paramMap.has('habitacionesId')) {
          return;
        }
        const userId = paramMap.get('userId');
        this.user = this.loginService.getUser(userId);
      });
    }
  }
  logOut() {
    this.loginService.logOut();
  }
}
