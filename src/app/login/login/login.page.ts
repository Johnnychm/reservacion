import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, UrlSegment } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { CatalogoService } from 'src/app/catalogo/catalogo.service';
import { User } from '../login.model';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  formExample: FormGroup;
  user: User;

  constructor(
    private router: Router,
    private loginService: LoginService,
    private alert: AlertController,
    private catalogoService: CatalogoService
  ) {}

  ngOnInit() {
    this.formExample = new FormGroup({
      email: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.email],
      }),
      password: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required],
      }),
    });
  }

  logIn() {
    if (this.formExample.valid) {
      this.loginService
        .logIn(this.formExample.value.email, this.formExample.value.password)
        .pipe()
        .subscribe((res) => {
          console.log('llego1', res);
          const userKey = Object.keys(res).find(userId => res[userId].email === this.formExample.value.email);
          const user = userKey && res[userKey];
          const isValidUser =
            user?.password === this.formExample.value.password.toString();
          console.log('Llego', isValidUser);
          console.log('Llego', this.formExample.value.password);

          if (!isValidUser) {
            this.alert
              .create({
                header: 'Cuidado',
                message: 'Usuario o contraseña invalido',
                buttons: [
                  {
                    text: 'Confirmar',
                    role: 'cancel',
                  },
                ],
              })
              .then((alert) => {
                alert.present();
              });
          } else {
            this.catalogoService.getAll();
            setTimeout(() => {
              this.router.navigate(['/catalogo']);
            }, 500);
          }
        });
    }
  }
}
