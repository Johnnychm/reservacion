import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { User } from '../login.model';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.page.html',
  styleUrls: ['./agregar.page.scss'],
})
export class AgregarPage implements OnInit {
  formExample: FormGroup;
  users: User[];

  constructor(
    private router: Router,
    private loginService: LoginService,
    private alert: AlertController,
    private nav: NavController
  ) {}

  ngOnInit() {
    this.formExample = new FormGroup({
      nombre: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required],
      }),
      email: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.email],
      }),
      password: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required],
      }),
      rol: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required],
      }),
    });
  }
  registerFunction() {
    console.log(this.formExample);
    if (!this.formExample.valid) {
      return;
    }
    this.loginService.registerUser(
      '1',
      this.formExample.value.nombre,
      this.formExample.value.email,
      this.formExample.value.password,
      this.formExample.value.rol
    );
    this.loginService.getAll();
    setTimeout(() => {
      this.router.navigate(['/catalogo']);
    }, 500);
  }
}
