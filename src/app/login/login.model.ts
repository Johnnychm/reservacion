// interface dicta las reglas de la clase
export interface User {
  id: string;
  nombre: string;
  email: string;
  password: string;
  rol: string;
  active?: boolean;
}
export class User {
  constructor(
    public id: string,
    public nombre: string,
    public email: string,
    public password: string,
    public rol: string,
    public active?: boolean
  ) {} //los contrutores se daclaran los atributos que vamos usar
}
