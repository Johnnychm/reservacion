import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './login.model';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  public loggedUser: User;
  private users: User[] = [];

  constructor(private httpClient: HttpClient, private router: Router) {
    this.users = this.getAll();
  }

  getAll() {
    this.httpClient
      .get<{ [Key: string]: User }>(
        'https://reservacion-56978-default-rtdb.firebaseio.com/users.json'
      )
      .subscribe((restData) => {
        const users = [];
        for (const key in restData) {
          if (restData.hasOwnProperty(key)) {
            users.push(
              new User(
                key,
                restData[key].nombre,
                restData[key].email,
                restData[key].password,
                restData[key].rol
              )
            );
          }
        }
        this.users = users;
      });
    return [...this.users];
  }

  getUser(userId: string) {
    return {
      ...this.users.find((user) => userId === user.id),
    };
  }

  registerUser(
    id: string,
    nombre: string,
    email: string,
    password: string,
    rol: string
  ) {
    id = Math.random().toString();
    const newUser = new User(id, nombre, email, password, rol);
    this.httpClient
      .post(
        'https://reservacion-56978-default-rtdb.firebaseio.com/users.json',
        {
          ...newUser,
          id: null,
        }
      )
      .subscribe((respData) => {
        console.log('usuario agregado');
      });
    this.users.push(newUser);
    console.log(this.users);
  }
  logIn(email: string, password: string): Observable<User> {
    console.log();
    return this.httpClient
      .get<User>(
        `https://reservacion-56978-default-rtdb.firebaseio.com/users.json?email="${email}"`
      );
    //  .pipe(() => console.log('User fetched'));
    // .subscribe((respDatos: User) => {
    //   console.log('GET USER', respDatos);
    //   const active = respDatos.password === password;
    //   this.loggedUser = { ...respDatos, };
    // });
   // return { ...this.loggedUser };
  }
  logOut() {
    this.loggedUser = undefined;
    this.router.navigate(['/login/login']);
  }
}
