import { Component, OnInit } from '@angular/core';
import { Habitacion } from './catalogo.model';
import { CatalogoService } from './catalogo.service';

@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.page.html',
  styleUrls: ['./catalogo.page.scss'],
})
export class CatalogoPage implements OnInit {
  habitaciones: Habitacion[];
  constructor(private catalogoServicio: CatalogoService) {} // llamando y use a Catalogo service

  ngOnInit() {
    console.log('Entro');
    this.habitaciones = this.catalogoServicio.getAll();
  }
  ionViewWillEnter(){
    setTimeout(() => {
    console.log('Entro al will enter');
    console.log('Entro al will enter');
    this.habitaciones = this.catalogoServicio.getAll();
  }, 150);
  }
  doRefresh(event){
    setTimeout(() => {
      this.habitaciones = this.catalogoServicio.getAll();
      event.target.complete();
    }, 1000);
  }


}
