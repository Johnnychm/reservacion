/* eslint-disable max-len */
/* eslint-disable arrow-body-style */
//Service vamos a manera un tipo de crud//
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Habitacion } from './catalogo.model';

@Injectable({
  providedIn: 'root',
})
export class CatalogoService {
  private habitacion: Habitacion;
  private habitaciones: Habitacion[] = [
    // {
    //   id: 'Hab-1',
    //   titulo: 'Habitacion Grande',
    //   descripcion:
    //     ' Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quidem provident consectetur vel libero similique quam assumenda quod minima',
    //   img: 'https://www.stanzahotel.com/wp-content/uploads/2020/07/2020_stanza_hotel_habitacion_sencilla_01.jpg',
    // },
    // {
    //   id: 'Hab-2',
    //   titulo: 'Habitacion Mediana',
    //   descripcion:
    //     ' Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quidem provident consectetur vel libero similique quam assumenda quod minima',
    //   img: 'https://www.cataloniahotels.com/es/blog/wp-content/uploads/2016/05/habitaci%C3%B3n-doble-catalonia-620x412.jpg',
    // },
    // {
    //   id: 'Hab-2',
    //   titulo: 'Habitacion pequeña',
    //   descripcion:
    //     ' Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quidem provident consectetur vel libero similique quam assumenda quod minima',
    //   img: 'https://wonder-day.com/wp-content/uploads/2020/10/wonder-day-among-us-4.png',
    // },
  ];
  constructor(
    private httpClient: HttpClient //se inicializa un variable de tipo httpclient
  ) {}
  getAll() {
    this.httpClient
    .get(
      'https://reservacion-56978-default-rtdb.firebaseio.com/habitacion.json?orderBy="active"&equalTo=true',
    )
    .subscribe(
      (respDatos) => {
        this.habitaciones = Object.keys(respDatos).map(id => ({ id, ...respDatos[id]}) );
      console.log('GET ALL HABITACIONES',respDatos);
    },
   );
   return [...this.habitaciones];
  }
  getHabitacion(catalogoId: string) {
    console.log(catalogoId);
    this.httpClient
    .get(`https://reservacion-56978-default-rtdb.firebaseio.com/habitacion/${catalogoId}.json`,
    )
    .subscribe(
      (respDatos) => {
      console.log('GET HABITACIONE',respDatos);
      this.habitacion = {id: catalogoId,...respDatos} as Habitacion;
    },
   );
   return {...this.habitacion};
  }
  eliminarHab() {}

  agregarHab(
    id: string,
    titulo: string,
    descripcion: string,
    ocupacion: number,
    precio: number,
    img: string
  ) {
    const newHabitacion = new Habitacion(
      true,
      id,
      titulo,
      descripcion,
      ocupacion,
      precio,
      img

    );
    // Las colecciones son como tablas por se agrega el link mas el nombre de la coleccion ejem: Habitacion.json
    this.httpClient
      .post(
        'https://reservacion-56978-default-rtdb.firebaseio.com/habitacion.json',
        {
          ...newHabitacion, // los tres puntos envia una copia de la habitacion
          id: null,
        }
      )
      .subscribe(
        (respDatos) => {
        console.log('Habitacion agregada');
      },
     );
    this.habitaciones.push(newHabitacion);
    console.log(this.habitaciones);
  }
  editProduct(habitacion: Habitacion) {
    console.log('edit habitacion', habitacion);
    this.httpClient.put<{name: string}>(`https://reservacion-56978-default-rtdb.firebaseio.com/habitacion/${habitacion.id}.json`, {

    ...habitacion,
      id: null
    }).subscribe(
      (restData) =>{
        console.log(restData);
        this.habitacion = habitacion;

      }
    );
    const habitacionIndex = this.habitaciones.findIndex(h=> h.id===habitacion.id);
        if (habitacionIndex>-1){
          const newHabitaciones = [...this.habitaciones];
          newHabitaciones[habitacionIndex] = habitacion;
          this.habitaciones=newHabitaciones;
        }
  }
}
