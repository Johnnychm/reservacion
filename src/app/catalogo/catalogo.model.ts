// interface dicta las reglas de la clase
export interface Habitacion {
  id: string;
  titulo: string;
  descripcion: string;
  img: string;
}
export class Habitacion{
  constructor(
    public active: boolean,
    public id: string,
    public titulo: string,
    public descripcion: string,
    public ocupacion: number,
    public precio: number,
    public img: string,
    ){}  //los contrutores se daclaran los atributos que vamos usar
}
