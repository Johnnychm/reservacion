import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Habitacion } from '../catalogo.model';
import { CatalogoService } from '../catalogo.service';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.page.html',
  styleUrls: ['./editar.page.scss'],
})
export class EditarPage implements OnInit {
  habitacion: Habitacion;
  formExample: FormGroup;
  ishidden = false;


  constructor(
    private activetedRoute: ActivatedRoute,
    private catalogoServicio: CatalogoService,
    private router: Router
  ) {}

  ngOnInit() {
    this.activetedRoute.paramMap.subscribe(
      // funcion subscribe / mapeo de datos//
      (paramMap) => {
        if (!paramMap.has('catalogoId')) {
          //No existe el parametro redirecciono//
          return;
        }
        const habitacionId = paramMap.get('catalogoId');
        const habitacion = this.catalogoServicio.getHabitacion(habitacionId);
        console.log('ngOnInit', habitacion);

        if (habitacion) {
          this.habitacion = habitacion;
        }
      }
    );
    this.formExample = new FormGroup({
      id: new FormControl(this.habitacion.id, {
        updateOn: 'blur',
        validators: [Validators.required],
      }),
      titulo: new FormControl(this.habitacion.titulo, {
        updateOn: 'blur',
        validators: [Validators.required],
      }), // inicializar cada unas de la validaciones requeridas
      descripcion: new FormControl(this.habitacion.descripcion, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.maxLength(255)],
      }),
      ocupacion: new FormControl(this.habitacion.ocupacion, {
        updateOn: 'blur',
        validators: [],
      }),
      precio: new FormControl(this.habitacion.precio, {
        updateOn: 'blur',
        validators: [],
      }),
      img: new FormControl(this.habitacion.img, {
        updateOn: 'blur',
        validators: [],
      }),
      active: new FormControl(this.habitacion.active, {
        updateOn: 'change',
        validators: [],
      }),
    });
  }
  ionViewWillEnter() {
    setTimeout(() => {
      this.activetedRoute.paramMap.subscribe(
        // funcion subscribe / mapeo de datos//
        (paramMap) => {
          if (!paramMap.has('catalogoId')) {
            //No existe el parametro redirecciono//
            return;
          }
          const habitacionId = paramMap.get('catalogoId');
          const habitacion = this.catalogoServicio.getHabitacion(habitacionId);
          console.log('ngOnInit', habitacion);

          if (habitacion) {
            this.habitacion = habitacion;
          }
        }
      );
    }, 150);
  }

  doRefresh(event) {
    setTimeout(() => {
      this.activetedRoute.paramMap.subscribe(
        // funcion subscribe / mapeo de datos//
        (paramMap) => {
          if (!paramMap.has('catalogoId')) {
            //No existe el parametro redirecciono//
            return;
          }
          const habitacionId = paramMap.get('catalogoId');
          const habitacion = this.catalogoServicio.getHabitacion(habitacionId);
          console.log('ngOnInit', habitacion);

          if (habitacion) {
            this.habitacion = habitacion;
          }
        }
      );
      event.target.complete();
    }, 1000);
  }

  editFunction() {
    if (!this.formExample.valid) {
      return;
    }
    this.catalogoServicio.editProduct({
      titulo: this.formExample.value.titulo,
      id: this.formExample.value.id,
      descripcion: this.formExample.value.descripcion,
      ocupacion: this.formExample.value.ocupacion,
      precio: this.formExample.value.precio,
      img: '',
      active: this.formExample.value.active,
    });
    this.router.navigate(['/catalogo']);
  }
}
