import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Habitacion } from '../catalogo.model';
import { CatalogoService } from '../catalogo.service';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})
export class DetallePage implements OnInit {
  habitacion: Habitacion = {id:'', titulo:'', descripcion:'', ocupacion:0, precio:0, img:'', active: true};
  constructor(
    private activetedRoute: ActivatedRoute,
    private catalogoServicio: CatalogoService
  ) {}

  ngOnInit() {

    this.activetedRoute.paramMap.subscribe(
      // funcion subscribe / mapeo de datos//
      paramMap => {
        if (!paramMap.has('catalogoId')){
          //No existe el parametro redirecciono//
          return;
        }
        const habitacionId = paramMap.get('catalogoId');
        const habitacion = this.catalogoServicio.getHabitacion(habitacionId);
        console.log('ngOnInit',habitacion );

        if (habitacion) {
          this.habitacion = habitacion;
        }
      }
    );
  }

  ionViewWillEnter() {
    setTimeout(() => {
    this.activetedRoute.paramMap.subscribe(
      // funcion subscribe / mapeo de datos//
      paramMap => {
        if (!paramMap.has('catalogoId')){
          //No existe el parametro redirecciono//
          return;
        }
        const habitacionId = paramMap.get('catalogoId');
        const habitacion = this.catalogoServicio.getHabitacion(habitacionId);
        console.log('ngOnInit',habitacion );

        if (habitacion) {
          this.habitacion = habitacion;
        }
      }
    );
  }, 150);
  }

  doRefresh(event){
    setTimeout(() => {
      this.activetedRoute.paramMap.subscribe(
        // funcion subscribe / mapeo de datos//
        paramMap => {
          if (!paramMap.has('catalogoId')){
            //No existe el parametro redirecciono//
            return;
          }
          const habitacionId = paramMap.get('catalogoId');
          const habitacion = this.catalogoServicio.getHabitacion(habitacionId);
          console.log('ngOnInit',habitacion );

          if (habitacion) {
            this.habitacion = habitacion;
          }
        }
      );
      event.target.complete();
    }, 1000);
  }


}
