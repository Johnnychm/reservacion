import { Component, OnInit } from '@angular/core';
import {FormControl,FormGroup,Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { CatalogoService } from '../catalogo.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.page.html',
  styleUrls: ['./agregar.page.scss'],
})
export class AgregarPage implements OnInit {
  formExample: FormGroup; //Agrupar todos lo elementos del formulario y sincronizar con el backend con la funcion addfunction

  constructor(
    private habitacionService: CatalogoService,
    private router: Router
  ) {}

  ngOnInit() {
    this.formExample = new FormGroup({
      id: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required],
      }),
      titulo: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required],
      }),
      descripcion: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required],
      }),
      ocupacion: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required],
      }),
      precio: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required],
      }),
      img: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required],
      }),
    });
  }
  funcionAgregar() {
    console.log(this.formExample);
    if(!this.formExample.valid){
      return;
    }
    console.log(this.formExample);
    this.habitacionService.agregarHab(
      this.formExample.value.id,
      this.formExample.value.titulo,
      this.formExample.value.descripcion,
      this.formExample.value.ocupacion,
      this.formExample.value.precio,
      this.formExample.value.img
      );
      this.router.navigate(['/catalogo']);
  }
}
